#!/usr/bin/env php
<?php
// application.php

require __DIR__ . '/vendor/autoload.php';

use Deliverea\CoffeeMachine\DrinkMachine\Order\Infrastructure\DrinkOrderCreatorCommand;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\CreateOrderUseCaseAutoInject;
use Symfony\Component\Console\Application;

$createOrderUseCase = CreateOrderUseCaseAutoInject::new();
$application       = new Application();
$application->add(new DrinkOrderCreatorCommand($createOrderUseCase));

$application->run();
