<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Price\Domain;

final class DrinkPriceEnum
{
    const TEA_PRICE       = 0.4;
    const COFFEE_PRICE    = 0.5;
    const CHOCOLATE_PRICE = 0.6;
}