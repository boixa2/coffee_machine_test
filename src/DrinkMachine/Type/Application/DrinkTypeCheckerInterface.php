<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Type\Application;

use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\Exceptions\DrinkTypeException;

interface DrinkTypeCheckerInterface
{
    /**
     * @param string $drinkType
     * @return bool
     * @throws DrinkTypeException
     */
    public function typeExists(string $drinkType) : bool ;
}