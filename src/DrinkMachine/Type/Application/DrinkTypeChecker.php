<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Type\Application;

use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkTypeEnum;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\Exceptions\DrinkTypeException;

final class DrinkTypeChecker implements DrinkTypeCheckerInterface
{

    /**
     * @param string $drinkType
     * @return bool
     * @throws DrinkTypeException
     */
    public function typeExists(string $drinkType): bool
    {
        if (in_array($drinkType, DrinkTypeEnum::allTypes())) {
            return true;
        }

        throw DrinkTypeException::factory();
    }
}