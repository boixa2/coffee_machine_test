<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Type\Application;

use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkInterface;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\Exceptions\DrinkTypeException;

interface DrinkTypeProviderInterface
{
    /**
     * @param string $drinkType
     * @return DrinkInterface
     * @throws DrinkTypeException
     */
    public function getEntityFromDrinkType(string $drinkType) : DrinkInterface;
}