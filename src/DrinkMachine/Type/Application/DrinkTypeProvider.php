<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Type\Application;

use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkFactoryInterface;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkInterface;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\Exceptions\DrinkTypeException;

final class DrinkTypeProvider implements DrinkTypeProviderInterface
{
    /** @var DrinkTypeCheckerInterface */
    private $drinkTypeChecker;

    /** @var DrinkFactoryInterface */
    private $drinkFactory;

    /**
     * @param DrinkTypeCheckerInterface $drinkTypeChecker
     * @param DrinkFactoryInterface $drinkFactory
     */
    public function __construct(DrinkTypeCheckerInterface $drinkTypeChecker, DrinkFactoryInterface $drinkFactory)
    {
        $this->drinkTypeChecker = $drinkTypeChecker;
        $this->drinkFactory     = $drinkFactory;
    }

    /**
     * @param string $drinkType
     * @return DrinkInterface
     * @throws DrinkTypeException
     */
    public function getEntityFromDrinkType(string $drinkType): DrinkInterface
    {
        if ($this->drinkTypeChecker->typeExists($drinkType)) {
            return $this->drinkFactory->instanceFromType($drinkType);
        }
    }
}