<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Type\Domain;

use Deliverea\CoffeeMachine\DrinkMachine\Price\Domain\DrinkPriceEnum;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkInterface;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkTypeEnum;

final class TeaVO implements DrinkInterface
{
    public function getPrice(): float
    {
        return DrinkPriceEnum::TEA_PRICE;
    }

    public function getName(): string
    {
        return DrinkTypeEnum::TEA;
    }
}