<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Type\Domain;

use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkInterface;
use Deliverea\CoffeeMachine\Drink\DrinkType\Exceptions\DrinkTypeException;

interface DrinkFactoryInterface
{
    /**
     * @param string $drinkType
     * @return DrinkInterface
     * @throws DrinkTypeException
     */
    public function instanceFromType(string $drinkType) : DrinkInterface;
}