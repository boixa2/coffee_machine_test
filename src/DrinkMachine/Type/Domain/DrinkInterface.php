<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Type\Domain;

interface DrinkInterface
{
    /**
     * @return float
     */
    public function getPrice() : float ;

    /**
     * @return string
     */
    public function getName() : string ;
}