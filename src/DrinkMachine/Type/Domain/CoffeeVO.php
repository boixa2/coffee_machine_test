<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Type\Domain;

use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkInterface;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkTypeEnum;
use Deliverea\CoffeeMachine\DrinkMachine\Price\Domain\DrinkPriceEnum;

final class CoffeeVO implements DrinkInterface
{
    public function getPrice(): float
    {
        return DrinkPriceEnum::COFFEE_PRICE;
    }

    public function getName(): string
    {
        return DrinkTypeEnum::COFFEE;
    }
}