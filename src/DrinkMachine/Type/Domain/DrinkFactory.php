<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Type\Domain;

use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkFactoryInterface;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkInterface;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkTypeEnum;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\Exceptions\DrinkTypeException;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\TeaVO;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\ChocolateVO;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\CoffeeVO;

final class DrinkFactory implements DrinkFactoryInterface
{
    /**
     * @param string $drinkType
     * @return DrinkInterface
     * @throws DrinkTypeException
     */
    public function instanceFromType(string $drinkType) : DrinkInterface
    {
        switch ($drinkType) {
            case DrinkTypeEnum::TEA:
                return new TeaVO();
            case DrinkTypeEnum::COFFEE:
                return new CoffeeVO();
            case DrinkTypeEnum::CHOCOLATE:
                return new ChocolateVO();
            default:
                throw DrinkTypeException::factory();
        }
    }
}