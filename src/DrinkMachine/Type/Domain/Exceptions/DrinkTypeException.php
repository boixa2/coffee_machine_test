<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\Exceptions;

final class DrinkTypeException extends \Exception
{
    const DRINK_TYPE_MUST_EXCEPTION_MESSAGE = 'The drink type should be tea, coffee or chocolate.';

    public static function factory(string $messageError = self::DRINK_TYPE_MUST_EXCEPTION_MESSAGE) : DrinkTypeException
    {
        return new DrinkTypeException($messageError);
    }
}