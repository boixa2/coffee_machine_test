<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Type\Domain;

final class DrinkTypeEnum
{
    const TEA       = 'tea';
    const COFFEE    = 'coffee';
    const CHOCOLATE = 'chocolate';

    /**
     * @return array
     */
    public static function allTypes(): array
    {
        return [
            self::TEA,
            self::COFFEE,
            self::CHOCOLATE
        ];
    }

}