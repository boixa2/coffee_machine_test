<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Type\Domain;

use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkInterface;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkTypeEnum;
use Deliverea\CoffeeMachine\DrinkMachine\Price\Domain\DrinkPriceEnum;

final class ChocolateVO implements DrinkInterface
{
    public function getPrice(): float
    {
        return DrinkPriceEnum::CHOCOLATE_PRICE;
    }

    public function getName(): string
    {
        return DrinkTypeEnum::CHOCOLATE;
    }
}