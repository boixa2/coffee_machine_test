<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Payment;

use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Payment\Exceptions\NotEnoughMoneyException;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkInterface;

interface OrderPaymentCheckerInterface
{
    /**
     * @param float $payment
     * @param DrinkInterface $drink
     * @return void
     * @throws NotEnoughMoneyException
     */
    public function paymentReachDrinkPrice(float $payment, DrinkInterface $drink) : void ;
}