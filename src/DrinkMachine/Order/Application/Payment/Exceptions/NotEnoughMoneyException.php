<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Payment\Exceptions;

use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkInterface;

final class NotEnoughMoneyException extends \Exception
{
    /**
     * @param DrinkInterface $drink
     * @return NotEnoughMoneyException
     */
    public static function factory(DrinkInterface $drink) : NotEnoughMoneyException
    {
        $errorMessage = 'The ' . $drink->getName() . ' costs ' . $drink->getPrice() . '.';
        return new NotEnoughMoneyException($errorMessage);
    }
}