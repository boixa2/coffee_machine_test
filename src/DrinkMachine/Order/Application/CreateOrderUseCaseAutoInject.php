<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Order\Application;

use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Sugar\SugarChecker;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Domain\OrderFactory;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Application\DrinkTypeChecker;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Application\DrinkTypeProvider;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkFactory;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Message\OrderSuccessMessageBuilder;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Payment\OrderPaymentChecker;

final class CreateOrderUseCaseAutoInject
{
    public static function new() : CreateOrderUseCaseInterface
    {
        $drinkTypeChecker    = new DrinkTypeChecker();
        $drinkFactory        = new DrinkFactory();
        $drinkTypeProvider   = new DrinkTypeProvider($drinkTypeChecker, $drinkFactory);
        $orderPaymentChecker = new OrderPaymentChecker();
        $sugarChecker        = new SugarChecker();
        $orderFactory        = new OrderFactory();
        $orderMessageFactory = new OrderSuccessMessageBuilder($sugarChecker);

        return new CreateOrderUseCase($orderFactory, $orderMessageFactory, $drinkTypeProvider, $orderPaymentChecker, $sugarChecker);
    }
}