<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Order\Application;

use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Payment\Exceptions\NotEnoughMoneyException;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Sugar\Exceptions\IncorrectSugarNumberException;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Sugar\SugarCheckerInterface;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Domain\OrderFactoryInterface;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Message\OrderSuccessMessageBuilderInterface;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Payment\OrderPaymentCheckerInterface;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Application\DrinkTypeProviderInterface;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\Exceptions\DrinkTypeException;

final class CreateOrderUseCase implements CreateOrderUseCaseInterface
{
    /** @var OrderFactoryInterface */
    private $orderFactory;

    /** @var OrderSuccessMessageBuilderInterface */
    private $orderSuccessMessageBuilder;

    /** @var DrinkTypeProviderInterface */
    private $drinkTypeProvider;

    /** @var OrderPaymentCheckerInterface */
    private $orderPaymentChecker;

    /** @var SugarCheckerInterface */
    private $sugarChecker;

    public function __construct(
        OrderFactoryInterface $orderFactory,
        OrderSuccessMessageBuilderInterface $orderSuccessMessageBuilder,
        DrinkTypeProviderInterface $drinkTypeProvider,
        OrderPaymentCheckerInterface $orderPaymentChecker,
        SugarCheckerInterface $sugarChecker
    ) {
        $this->orderFactory               = $orderFactory;
        $this->orderSuccessMessageBuilder = $orderSuccessMessageBuilder;
        $this->drinkTypeProvider          = $drinkTypeProvider;
        $this->orderPaymentChecker        = $orderPaymentChecker;
        $this->sugarChecker               = $sugarChecker;
    }

    /**
     * @param string $drinkType
     * @param float $money
     * @param int $sugars
     * @param bool|null $extraHot
     * @return string
     * @throws DrinkTypeException
     * @throws NotEnoughMoneyException
     * @throws IncorrectSugarNumberException
     */
    public function newDrinkOrder(string $drinkType, float $money, int $sugars, bool $extraHot = null): string
    {
        $drink = $this->drinkTypeProvider->getEntityFromDrinkType($drinkType);
        $this->orderPaymentChecker->paymentReachDrinkPrice($money, $drink);
        $this->sugarChecker->checkSugarParameter($sugars);
        $order = $this->orderFactory->newOrder($drink, $sugars, $extraHot === true);

        return $this->orderSuccessMessageBuilder->buildMessage($order);
    }
}