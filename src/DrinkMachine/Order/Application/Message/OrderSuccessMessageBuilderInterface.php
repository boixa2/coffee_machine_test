<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Message;

use Deliverea\CoffeeMachine\DrinkMachine\Order\Domain\Entity\OrderInterface;

interface OrderSuccessMessageBuilderInterface
{
    public function buildMessage(OrderInterface $order) : string ;
}