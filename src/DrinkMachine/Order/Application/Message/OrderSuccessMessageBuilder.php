<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Message;

use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Sugar\SugarCheckerInterface;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Domain\Entity\OrderInterface;

final class OrderSuccessMessageBuilder implements OrderSuccessMessageBuilderInterface
{
    const YOU_ORDERED_MESSAGE = 'You have ordered a ';
    const EXTRA_HOT_MESSAGE   = ' extra hot';

    /** @var SugarCheckerInterface */
    private $sugarChecker;

    /** @var string */
    private $successMessage;

    /**
     * @param SugarCheckerInterface $sugarChecker
     */
    public function __construct(SugarCheckerInterface $sugarChecker)
    {
        $this->sugarChecker = $sugarChecker;
        $this->successMessage = self::YOU_ORDERED_MESSAGE;
    }

    /**
     * @param OrderInterface $order
     * @return string
     */
    public function buildMessage(OrderInterface $order): string
    {
        $this->successMessage .= $order->getDrink()->getName();

        if ($order->isHot()) {
            $this->addExtraHotMessage();
        }

        if ($this->sugarChecker->mustAddSugarAndStick($order->sugars())) {
            $this->addStickAndSugarsMessage($order->sugars());
        }

        return $this->successMessage;
    }

    private function addExtraHotMessage()
    {
        $this->successMessage .= self::EXTRA_HOT_MESSAGE;
    }

    private function addStickAndSugarsMessage(int $sugars)
    {
        $this->successMessage .= " with $sugars sugars (stick included)";
    }
}