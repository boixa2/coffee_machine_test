<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Sugar\Exceptions;

class SugarTypeException extends \Exception
{
    const SUGAR_TYPE_ERROR_MESSAGE = 'The number of sugars is not an integer.';

    public static function factory() : SugarTypeException
    {
        return new SugarTypeException(self::SUGAR_TYPE_ERROR_MESSAGE);
    }

}