<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Sugar\Exceptions;

use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Sugar\SugarChecker;

class IncorrectSugarNumberException extends \Exception
{
    const SUGAR_NUMBER_ERROR_MESSAGE = 'The number of sugars should be between '. SugarChecker::MINIMUM_SUGAR_NUMBER .' and '. SugarChecker::MAXIMUM_SUGAR_NUMBER .'.';

    public static function factory($errorMessage = self::SUGAR_NUMBER_ERROR_MESSAGE) : IncorrectSugarNumberException
    {
        return new IncorrectSugarNumberException($errorMessage);
    }
}