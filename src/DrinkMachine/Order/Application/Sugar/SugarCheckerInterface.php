<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Sugar;

use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Sugar\Exceptions\IncorrectSugarNumberException;

interface SugarCheckerInterface
{
    /**
     * @param string $sugarParameter
     * @throws IncorrectSugarNumberException
     */
    public function checkSugarParameter(string $sugarParameter) : void ;

    /**
     * @param int $sugarNumber
     * @return bool
     */
    public function checkSugarNumber(int $sugarNumber) : bool ;

    /**
     * @param int $sugarNumber
     * @return bool
     */
    public function mustAddSugarAndStick(int $sugarNumber) : bool ;
}