<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Sugar;

use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Sugar\Exceptions\IncorrectSugarNumberException;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Sugar\Exceptions\SugarTypeException;

final class SugarChecker implements SugarCheckerInterface
{
    const MINIMUM_SUGAR_NUMBER = 0;
    const MAXIMUM_SUGAR_NUMBER = 2;

    /**
     * @param string $sugarParameter
     * @throws IncorrectSugarNumberException
     * @throws SugarTypeException
     */
    public function checkSugarParameter(string $sugarParameter): void
    {
        $sugars = $this->sugarToInteger($sugarParameter);
        if ($this->checkSugarNumber($sugars)) {
            return;
        }
    }

    /**
     * @param string $sugar
     * @return int
     * @throws SugarTypeException
     */
    private function sugarToInteger(string $sugar) : int
    {
        if (is_numeric($sugar)) {
            return intval($sugar);
        }

        throw SugarTypeException::factory();
    }

    /**
     * @param int $sugars
     * @return bool
     * @throws IncorrectSugarNumberException
     */
    public function checkSugarNumber(int $sugars): bool
    {
        if ($sugars >= self::MINIMUM_SUGAR_NUMBER && $sugars <= self::MAXIMUM_SUGAR_NUMBER) {
            return true;
        }

        throw IncorrectSugarNumberException::factory();
    }

    /**
     * @param int $sugarNumber
     * @return bool
     */
    public function mustAddSugarAndStick(int $sugarNumber): bool
    {
        return $sugarNumber > self::MINIMUM_SUGAR_NUMBER;
    }
}