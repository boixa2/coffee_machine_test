<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Order\Application;

use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\Exceptions\DrinkTypeException;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Payment\Exceptions\NotEnoughMoneyException;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Sugar\Exceptions\IncorrectSugarNumberException;

interface CreateOrderUseCaseInterface
{
    /**
     * @param string $drinkType
     * @param float $money
     * @param int $sugars
     * @param bool|null $extraHot
     * @return string
     * @throws DrinkTypeException
     * @throws NotEnoughMoneyException
     * @throws IncorrectSugarNumberException
     */
    public function newDrinkOrder(string $drinkType, float $money, int $sugars, bool $extraHot) : string;
}