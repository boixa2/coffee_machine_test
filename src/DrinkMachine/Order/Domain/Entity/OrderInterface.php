<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Order\Domain\Entity;

use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkInterface;

interface OrderInterface
{
    /**
     * @return DrinkInterface
     */
    public function getDrink(): DrinkInterface;

    /**
     * @return bool
     */
    public function isHot(): bool;

    /**
     * @return int
     */
    public function sugars(): int;
}