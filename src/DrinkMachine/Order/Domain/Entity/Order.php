<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Order\Domain\Entity;

use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkInterface;

final class Order implements OrderInterface
{
    /** @var DrinkInterface */
    private $drink;

    /** @var bool */
    private $hot;

    /** @var int */
    private $sugars;

    public function __construct(DrinkInterface $drink, int $sugars, bool $hot)
    {
        $this->drink  = $drink;
        $this->sugars = $sugars;
        $this->hot    = $hot;
    }

    public function getDrink(): DrinkInterface
    {
        return $this->drink;
    }

    public function isHot(): bool
    {
        return $this->hot;
    }

    public function sugars(): int
    {
        return $this->sugars;
    }
}