<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Order\Domain;

use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkInterface;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Domain\Entity\OrderInterface;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Sugar\Exceptions\IncorrectSugarNumberException;

interface OrderFactoryInterface
{

    /**
     * @param DrinkInterface $drink
     * @param int $sugar
     * @param bool $hot
     * @return OrderInterface
     * @throws IncorrectSugarNumberException
     */
    public function newOrder(DrinkInterface $drink, int $sugar, bool $hot) : OrderInterface;
}