<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Order\Domain;

use Deliverea\CoffeeMachine\DrinkMachine\Order\Domain\Entity\Order;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Domain\Entity\OrderInterface;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkInterface;

final class OrderFactory implements OrderFactoryInterface
{

    /**
     * @param DrinkInterface $drink
     * @param int $sugar
     * @param bool $hot
     * @return OrderInterface
     */
    public function newOrder(DrinkInterface $drink, int $sugar, bool $hot): OrderInterface
    {
        return new Order($drink, $sugar, $hot);
    }

}