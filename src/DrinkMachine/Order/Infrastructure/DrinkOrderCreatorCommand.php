<?php

namespace Deliverea\CoffeeMachine\DrinkMachine\Order\Infrastructure;

use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\CreateOrderUseCaseInterface;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\Exceptions\DrinkTypeException;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Payment\Exceptions\NotEnoughMoneyException;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Sugar\Exceptions\IncorrectSugarNumberException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class DrinkOrderCreatorCommand extends Command
{
    protected static $defaultName = 'app:order-drink';

    /** @var CreateOrderUseCaseInterface */
    private $createOrderUseCase;

    public function __construct(
        CreateOrderUseCaseInterface $createOrderUseCase
    ) {
        $this->createOrderUseCase = $createOrderUseCase;
        parent::__construct();
    }

    protected function configure()
    {
        $this->addArgument(
            'drink-type',
            InputArgument::REQUIRED,
            'The type of the drink. (Tea, Coffee or Chocolate)'
        );

        $this->addArgument(
            'money',
            InputArgument::REQUIRED,
            'The amount of money given by the user'
        );

        $this->addArgument(
            'sugars',
            InputArgument::OPTIONAL,
            'The number of sugars you want. (0, 1, 2)',
            0
        );

        $this->addOption(
            'extra-hot',
            'e',
            InputOption::VALUE_NONE,
            $description = 'If the user wants to make the drink extra hot'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $drinkType = strtolower($input->getArgument('drink-type'));
        $money     = $input->getArgument('money');
        $sugars    = $input->getArgument('sugars');
        $extraHot  = $input->getOption('extra-hot');

        try {
            $successMessage = $this->createOrderUseCase->newDrinkOrder($drinkType, $money, $sugars, $extraHot);
            $output->writeln($successMessage);
        } catch (DrinkTypeException | NotEnoughMoneyException | IncorrectSugarNumberException $exception) {
            $output->writeln($exception->getMessage());
        }
    }
}
