<?php

namespace Deliverea\CoffeeMachine\Tests\Unit\DrinkMachine\DrinkType;

use Deliverea\CoffeeMachine\DrinkMachine\Type\Application\DrinkTypeChecker;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkTypeEnum;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\Exceptions\DrinkTypeException;
use PHPUnit\Framework\TestCase;

class DrinkTypeCheckerTest extends TestCase
{
    /** @var DrinkTypeChecker */
    private $drinkTypeChecker;

    protected function setUp() : void
    {
        $this->drinkTypeChecker = new DrinkTypeChecker();
    }

    /**
     * @dataProvider provideDrinkTypes
     * @param string $drinkType
     * @throws DrinkTypeException
     */
    public function testSuccessfulDrinkType(string $drinkType)
    {
        $this->assertTrue($this->drinkTypeChecker->typeExists($drinkType));
    }

    /**
     * @throws DrinkTypeException
     */
    public function testNotExpectedDrinkType()
    {
        $this->expectException(DrinkTypeException::class);
        $this->drinkTypeChecker->typeExists('not_a_drink');
    }

    /**
     * @return array
     */
    public function provideDrinkTypes() : array
    {
        return [
            [DrinkTypeEnum::CHOCOLATE],
            [DrinkTypeEnum::TEA],
            [DrinkTypeEnum::COFFEE],
        ];
    }
}