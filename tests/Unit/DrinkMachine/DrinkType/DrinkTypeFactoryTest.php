<?php

namespace Deliverea\CoffeeMachine\Tests\Unit\DrinkMachine\DrinkType;

use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkFactory;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkInterface;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkTypeEnum;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\Exceptions\DrinkTypeException;
use PHPUnit\Framework\TestCase;

class DrinkTypeFactoryTest extends TestCase
{
    /** @var DrinkFactory */
    private $drinkTypeFactory;

    public function setUp() : void
    {
        $this->drinkTypeFactory = new DrinkFactory();
    }

    /**
     * @dataProvider provideAllEnumTypes
     * @param string $drinkType
     * @throws DrinkTypeException
     */
    public function testInstanceNewDrinks(string $drinkType)
    {
        $this->assertTrue($this->drinkTypeFactory->instanceFromType($drinkType) instanceof DrinkInterface);
    }

    /**
     * @return array
     */
    public function provideAllEnumTypes() : array
    {
        $allEnumTypes = [];
        foreach (DrinkTypeEnum::allTypes() as $drinkType) {
            $allEnumTypes[] = [$drinkType];
        }

        return $allEnumTypes;
    }

    /**
     * @throws DrinkTypeException
     */
    public function testIsNotDrinkInterface()
    {
        $this->expectException(DrinkTypeException::class);
        $this->expectExceptionMessage(DrinkTypeException::DRINK_TYPE_MUST_EXCEPTION_MESSAGE);
        $this->drinkTypeFactory->instanceFromType('random_type');
    }
}