<?php

namespace Deliverea\CoffeeMachine\Tests\Unit\Order\Payment;

use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Payment\Exceptions\NotEnoughMoneyException;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Payment\OrderPaymentChecker;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkFactory;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkInterface;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkTypeEnum;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\Exceptions\DrinkTypeException;
use PHPUnit\Framework\TestCase;

class OrderPaymentTest extends TestCase
{
    /** @var OrderPaymentChecker */
    private $orderPaymentChecker;

    /** @var DrinkInterface */
    private $tea;

    /**
     * @throws DrinkTypeException
     */
    protected function setUp() : void
    {
        $this->orderPaymentChecker = new OrderPaymentChecker();
        $drinkFactory              = new DrinkFactory();
        $this->tea                 = $drinkFactory->instanceFromType(DrinkTypeEnum::TEA);
    }

    /**
     * @throws NotEnoughMoneyException
     */
    public function testOrderPaymentSuccessful()
    {
        $this->assertNull($this->orderPaymentChecker->paymentReachDrinkPrice(10, $this->tea));
    }

    /**
     * @throws NotEnoughMoneyException
     */
    public function testOrderPaymentNotEnoughMoneyException()
    {
        $this->expectException(NotEnoughMoneyException::class);
        $this->orderPaymentChecker->paymentReachDrinkPrice(0.3, $this->tea);
    }
}