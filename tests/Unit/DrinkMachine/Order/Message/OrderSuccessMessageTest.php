<?php

namespace Deliverea\CoffeeMachine\Tests\Unit\Order\Message;

use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Message\OrderSuccessMessageBuilder;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Sugar\SugarChecker;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Domain\Entity\Order;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkFactory;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkTypeEnum;
use PHPUnit\Framework\TestCase;

class OrderSuccessMessageTest extends TestCase
{
    const TEA_HOT_ONE_SUGAR_EXPECTED_MESSAGE      = 'You have ordered a tea extra hot with 1 sugars (stick included)';
    const COFFEE_NO_HOT_NO_SUGAR_EXPECTED_MESSAGE = 'You have ordered a coffee';

    /**
     * @var SugarChecker
     */
    private $sugarChecker;

    /**
     * @var DrinkFactory
     */
    private $drinkFactory;

    protected function setUp() : void
    {
        $this->sugarChecker = new SugarChecker();
        $this->drinkFactory = new DrinkFactory();
    }

    public function testTeaHotWithOneSugarsSuccessMessages()
    {
        $teaHotOneSugarOrderSuccess = $this->orderTeaHotWithOneSugars();
        $this->assertEquals(self::TEA_HOT_ONE_SUGAR_EXPECTED_MESSAGE, $teaHotOneSugarOrderSuccess);
    }

    public function orderTeaHotWithOneSugars(): string
    {
        $drink               = $this->drinkFactory->instanceFromType(DrinkTypeEnum::TEA);
        $order               = new Order($drink, 1, true);
        $orderSuccessMessage = new OrderSuccessMessageBuilder($this->sugarChecker);
        return $orderSuccessMessage->buildMessage($order);
    }

    public function testCoffeeNoHotAndNoSugarSuccessMessages()
    {
        $coffeeNoHotNoSugarSuccess = $this->orderCoffeeNoHotAndNoSugar();
        $this->assertEquals(self::COFFEE_NO_HOT_NO_SUGAR_EXPECTED_MESSAGE, $coffeeNoHotNoSugarSuccess);
    }

    public function orderCoffeeNoHotAndNoSugar(): string
    {
        $drink               = $this->drinkFactory->instanceFromType(DrinkTypeEnum::COFFEE);
        $order               = new Order($drink, 0, false);
        $orderSuccessMessage = new OrderSuccessMessageBuilder($this->sugarChecker);
        return $orderSuccessMessage->buildMessage($order);
    }
}