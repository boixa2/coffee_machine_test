<?php

namespace Deliverea\CoffeeMachine\Tests\Unit\Order\Sugar;

use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Sugar\Exceptions\IncorrectSugarNumberException;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Sugar\Exceptions\SugarTypeException;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Sugar\SugarChecker;
use PHPUnit\Framework\TestCase;

class SugarCheckerTest extends TestCase
{
    const NO_SUGAR_CASE          = 0;
    const MAX_SUGARS_NUMBER      = 2;
    const ONE_SUGAR              = 1;
    const NEGATIVE_SUGARS_NUMBER = -1;
    const SO_MANY_SUGARS_NUMBER  = 3;
    const INCORRECT_SUGAR_TYPE   = 's';

    /** @var SugarChecker */
    private $sugarChecker;

    protected function setUp(): void
    {
        $this->sugarChecker = new SugarChecker();
    }

    public function testWithSugarSuccessful()
    {
        $this->assertTrue($this->sugarChecker->mustAddSugarAndStick(self::MAX_SUGARS_NUMBER));
        $this->assertTrue($this->sugarChecker->mustAddSugarAndStick(self::ONE_SUGAR));
    }

    public function testWithSugarParameterSuccessful()
    {
        $this->assertNull($this->sugarChecker->checkSugarParameter(strval(self::MAX_SUGARS_NUMBER)));
        $this->assertNull($this->sugarChecker->checkSugarParameter(strval(self::ONE_SUGAR)));
    }

    public function testEmptySugarSuccessful()
    {
        $this->assertFalse($this->sugarChecker->mustAddSugarAndStick(self::NO_SUGAR_CASE));
    }

    public function testSuccessSugarNumber()
    {
        $this->assertTrue($this->sugarChecker->checkSugarNumber(self::MAX_SUGARS_NUMBER));
    }

    public function testNoSugarsNumber()
    {
        $this->assertTrue($this->sugarChecker->checkSugarNumber(self::NO_SUGAR_CASE));
    }

    public function testNegativeIncorrectSugarNumber()
    {
        $this->expectException(IncorrectSugarNumberException::class);
        $this->expectExceptionMessage(IncorrectSugarNumberException::SUGAR_NUMBER_ERROR_MESSAGE);
        $this->sugarChecker->checkSugarParameter(self::NEGATIVE_SUGARS_NUMBER);
    }

    public function testSoManySugarsNumber()
    {
        $this->expectException(IncorrectSugarNumberException::class);
        $this->expectExceptionMessage(IncorrectSugarNumberException::SUGAR_NUMBER_ERROR_MESSAGE);
        $this->sugarChecker->checkSugarNumber(self::SO_MANY_SUGARS_NUMBER);
    }

    public function testIncorrectTypeSugarParameter()
    {
        $this->expectException(SugarTypeException::class);
        $this->expectExceptionMessage(SugarTypeException::SUGAR_TYPE_ERROR_MESSAGE);
        $this->sugarChecker->checkSugarParameter(self::INCORRECT_SUGAR_TYPE);
    }
}