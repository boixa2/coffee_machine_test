<?php

namespace Deliverea\CoffeeMachine\Tests\Integration\Console;

use Deliverea\CoffeeMachine\DrinkMachine\Order\Infrastructure\DrinkOrderCreatorCommand;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\CreateOrderUseCaseAutoInject;
use Deliverea\CoffeeMachine\DrinkMachine\Order\Application\Message\OrderSuccessMessageBuilder;
use Deliverea\CoffeeMachine\DrinkMachine\Type\Domain\DrinkTypeEnum;
use Deliverea\CoffeeMachine\Tests\Integration\IntegrationTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class MakeDrinkCommandTest extends IntegrationTestCase
{
    protected function setUp() : void
    {
        parent::setUp();

        $createOrderUseCaseAutoInject = CreateOrderUseCaseAutoInject::new();
        $this->application->add(new DrinkOrderCreatorCommand($createOrderUseCaseAutoInject));
    }

    /**
     * @dataProvider ordersProvider
     * @param string $drinkType
     * @param float $money
     * @param int $sugars
     * @param boolean $extraHot
     * @param string $expectedOutput
     */
    public function testCoffeeMachineReturnsTheExpectedOutput(
        string $drinkType,
        float $money,
        int $sugars,
        bool $extraHot,
        string $expectedOutput
    ): void {
        $command = $this->application->find('app:order-drink');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command'  => $command->getName(),

            // pass arguments to the helper
            'drink-type' => $drinkType,
            'money' => $money,
            'sugars' => $sugars,
            '--extra-hot' => $extraHot
        ));

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertSame($expectedOutput, $output);
    }

    /**
     * @return array
     */
    public function ordersProvider(): array
    {
        return [
            [
                DrinkTypeEnum::COFFEE, 0.7, 0, false, 'You have ordered a coffee' . PHP_EOL
            ],
            [
                DrinkTypeEnum::COFFEE, 0.7, 2, false, 'You have ordered a coffee with 2 sugars (stick included)' . PHP_EOL
            ],
            [
                DrinkTypeEnum::COFFEE, 0.7, 2, true, 'You have ordered a coffee extra hot with 2 sugars (stick included)' . PHP_EOL
            ],
            [
                DrinkTypeEnum::CHOCOLATE, 0.7, 1, false, 'You have ordered a chocolate with 1 sugars (stick included)' . PHP_EOL
            ],
            [
                DrinkTypeEnum::TEA, 0.4, 0, true, 'You have ordered a tea extra hot' . PHP_EOL
            ],
            [
                DrinkTypeEnum::COFFEE, 2, 2, true, 'You have ordered a coffee extra hot with 2 sugars (stick included)' . PHP_EOL
            ],
            [
                DrinkTypeEnum::COFFEE, 0.2, 2, true, 'The coffee costs 0.5.' . PHP_EOL
            ],
            [
                DrinkTypeEnum::CHOCOLATE, 0.3, 2, true, 'The chocolate costs 0.6.' . PHP_EOL
            ],
            [
                DrinkTypeEnum::TEA, 0.1, 2, true, 'The tea costs 0.4.' . PHP_EOL
            ],
            [
                DrinkTypeEnum::TEA, 0.5, -1, true, 'The number of sugars should be between 0 and 2.' . PHP_EOL
            ],
            [
                DrinkTypeEnum::TEA, 0.5, 3, true, 'The number of sugars should be between 0 and 2.' . PHP_EOL
            ],
            [
                DrinkTypeEnum::COFFEE, 0.5, 0, false, OrderSuccessMessageBuilder::YOU_ORDERED_MESSAGE . DrinkTypeEnum::COFFEE . PHP_EOL
            ],
            [
                DrinkTypeEnum::COFFEE, 0.5, 2, false, 'You have ordered a coffee with 2 sugars (stick included)' . PHP_EOL
            ],
            [
                DrinkTypeEnum::COFFEE, 0.5, 2, true, 'You have ordered a coffee extra hot with 2 sugars (stick included)' . PHP_EOL
            ]
        ];
    }
}
